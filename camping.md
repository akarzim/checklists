# Checklist Camping

> Éprouvée au [Grand Bal de l'Europe][Gennetines] (15 jours de festival)

[Gennetines]: https://www.gennetines.org/

## Checklist collective
### Hygiène

- Lessive
- Bassine
- Lingettes nettoyantes

### Équipement

- Appareil Photo + chargeur
- Piles de rechange pour l'éclairage
- Chargeur solaire
- Scotch américain
- Ficelle
- Préservatifs (sans dec’, c’est multi-usage !)
- Pinces à linge
- Miroir
- Couteau suisse / Opinel

### Divers

- Trousse à pharmacie (voir le détail) + huiles essentielles
- Sachets plastique / Ziplock
- Sacs poubelle
- Crème solaire
- PQ
- Jeux de société
- Café + Moulin + Aeropress
- Slackline
- Four solaire
- Hammac

## Checklist individuelle
### Camping

- Popote (couteau, fourchette)
- Tente
- Sac de rando
- Petit sac à dos
- Sac de couchage et/ou Drap de soie
- Matelas
- Cadenas  pour la tente
- Tarp

### Textile

- T-shirts + short/jupe + sous-vêtements pour 4 à 8 jours
- Vêtements chauds : pull, pantalon (les journées/nuits peuvent être fraîches)
- Sandales ou tongs
- Bottes
- Bonnet de nuit

### Hygiène

- Dentifrice + brosse à dents
- Savon / Shampooing solides
- Déodorant (merci pour les autres :P)
- Rasoir
- Serviette de bain microfibre

### Bouffe

- Collations : barres de céréales, fruits secs, graines
- Sandwiches (pour le premier jour)

### Équipement

- Lunettes de soleil
- Couvre-chef (bob, casquette, chapeau…)
- Étole
- Lampe de poche ou Frontale
- Mug rétractable
- K-way
- Pantalon de pluie ou
- Cape pour protéger personne + sac
- Téléphone + chargeur
- Sacs à vide d’air (pour le linge sale par exemple)

### Divers

- Prévoir de l'argent en liquide et/ou un chèque (au cas où votre CB est refusée)
- Carte d'identité + Permis de conduire
- Carte Vitale + Carte européenne d’assurance maladie
- Livre
- Accordéon, Guitare, …
- Siège pliant
