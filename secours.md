# Trousse de Secours

> **Guillaume :**
> « Wahoo, c'est une trousse de survie pour attaquer le mont blanc :D » — _juillet 2009_
>
> **Yoann :**
> “Bah si c'est prévu maintenant [le TMB]” — _juillet 2010_
 
 
## Petit matériel 

- Gel nettoyant pour les mains – 2 ou 3 paires de gants en latex
- Ciseaux
- Pince à écharde
- Épingles à nourrice
- Compresses stériles
- Coton
- Sparadrap prédécoupé
- Collant type URGOPORE
- Bandes nylon et bandes Velpo
- 1 thermomètre
- 1 couverture de survie
- 1 Aspivenin
- Purificateur d’eau — MICROPUR Forte

## Plaies

- Sérum physiologique, nettoyage des plaies [PHYSIODOSE]
- Antiseptique local, désinfection plaies [BISEPTINE] spray ou dosettes
- Antiseptique local, séchage des plaies [EOSINE] dosettes
- Sutures cutanées adhésives des plaies ouvertes [STERISTRIP]
- Plaies sales, brûlures [TULLEGRAS]
- Pommade antibiotique à usage local, petites plaies infectées [FUCIDINE] / [MUPIDERM]

[BISEPTINE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-obisep01-BISEPTINE.html
[EOSINE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-beoagb01-EOSINE-AQUEUSE-GILBERT.html
[FUCIDINE]:https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-bfucid01-FUCIDINE-creme-et-pommade.html
[MUPIDERM]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gf790011-MUPIDERM.html
[PHYSIODOSE]: https://eurekasante.vidal.fr/parapharmacie/vidal/produits-id11493n0-PHYSIODOSE-serum-physiologique.html
[STERISTRIP]: https://eurekasante.vidal.fr/parapharmacie/vidal/produits-vida4491-STERI-STRIP-3M.html
[TULLEGRAS]: https://eurekasante.vidal.fr/parapharmacie/vidal/produits-id8275-TULLEGRAS-M-S.html

## Douleurs et/ou fièvre

- Antalgique et antipyrétique [GELUPRANE] / [EFFERALGAN] / [DOLKO] / [DOLIPRANE] / [DAFALGAN] / [PARACÉTAMOL]
- Anti-inflammatoire [IBUPROFENE]

[DAFALGAN]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-odafal01-DAFALGAN.html
[DOLIPRANE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-ddolip01-DOLIPRANE.html
[DOLKO]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-odolko01-DOLKO.html
[EFFERALGAN]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp4735-EFFERALGAN-comprime.html
[GELUPRANE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-oprane01-GELUPRANE.html
[IBUPROFENE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp2694-IBUPROFENE-BIOGARAN-400-mg.html
[PARACÉTAMOL]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gf310009-PARACETAMOL-BIOGARAN.html

## Saignements de nez              

- Alginate de calcium [COALGAN]

[COALGAN]: https://eurekasante.vidal.fr/parapharmacie/vidal/produits-vidc0685-COALGAN.html

## Entorses

- Pommade anti-inflammatoire d'action locale [NIFLUGEL] / [NIFLURIL] / [VOLTARÈNE] / [ADVILGEL]
- Pomade homéopathique [ARNIGEL]

[ADVILGEL]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp5016-ADVILGEL.html
[ARNIGEL]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp724-ARNIGEL.html
[NIFLUGEL]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-oniflg01-NIFLUGEL.html
[NIFLURIL]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-jniflu01-NIFLURIL.html
[VOLTARÈNE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-jvolte01-VOLTARENE-EMULGEL.html

## Douleurs musculaires

- Bande auto-agrippante
- Réducteur de cicatrices [ELASTOPLAST]
- Chevillère, Genouillère

[ELASTOPLAST]: https://eurekasante.vidal.fr/parapharmacie/vidal/produits-id11091-ELASTOPLAST.html

## Contusions, hématomes

- Phytothérapie, pommade à l'Arnica [ARNICAGEL]

[ARNICAGEL]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp2689-ARNICAGEL.html

## Coups de soleil, brûlures cutanées légères

- Protecteur cutané [BIAFINE] / [LAMIDERM]

[BIAFINE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-bbiafi01-BIAFINE.html
[LAMIDERM]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp1060-LAMIDERM.html

## Piqûres d'insectes

- Pommade dermocorticoïde [DIPROSONE] / [NÉRISONE] / [LOCAPRED] / [LOCOÏD]

[DIPROSONE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-bdiprn01-DIPROSONE.html
[LOCAPRED]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-blocap01-LOCAPRED.html
[LOCOÏD]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-blocoi01-LOCOID.html
[NÉRISONE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-bneris01-NERISONE.html

## Démangeaisons

- Antiprurigineux local [HYDROCORTISONE]

[HYDROCORTISONE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-boncth01-ONCTOSE-HYDROCORTISONE.html

## Diarrhée

- Pansement digestif en sachets [SMECTA] / [ACTAPULGITE] / [BEDELIX]
- Antidiarrhéique [IMODIUM] / [LOPÉRAMIDE] / [ULTRA-LEVURE]

[ACTAPULGITE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-bactap01-ACTAPULGITE.html
[BEDELIX]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-bbedel01-BEDELIX.html
[IMODIUM]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-bimodi01-IMODIUM.html
[LOPÉRAMIDE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp5272-LOPERAMIDE-BIOGARAN.html
[SMECTA]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp6038-SMECTA-FRAISE.html
[ULTRA-LEVURE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-bultra01-ULTRA-LEVURE.html

## Constipation

- Laxatif [FORLAX] / [MACROGOL]

[FORLAX]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gf380001-FORLAX.html
[MACROGOL]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp3745-MACROGOL-BIOGARAN.html

## Douleurs intestinales

- Antispasmodique [SPASFON] / [DOMPÉRIDONE]

[DOMPÉRIDONE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp1155-DOMPERIDONE-BIOGARAN.html
[SPASFON]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-jspasf01-SPASFON.html

## Boutons de fièvre

- crème antiherpétique locale [ACICLOVIR] / [ZOVIRAX] / [CICALFATE]

[ACICLOVIR]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp1144-ACICLOVIR-BIOGARAN-creme.html
[CICALFATE]: https://eurekasante.vidal.fr/parapharmacie/vidal/produits-vf410015n2-CICALFATE-Creme.html
[ZOVIRAX]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gf380004-ZOVIRAX-creme-tube-de-2-g.html<Paste>

## Aphtes

- Préparation orobuccale à visée antiseptique [LYSO-6]

[LYSO-6]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-olyso601-LYSO-6.html

## Lèvres gercées

- Stick lèvres — [AVÈNE Stick lèvres hydratant][AVÈNE]

[AVÈNE]: https://eurekasante.vidal.fr/parapharmacie/vidal/produits-vf410015n184-AVENE-Stick-levres-hydratant.html

## Hydratation visage et mains

- Crème hydratante — [CERAVE] / [CICALFATE]

[CERAVE]: https://eurekasante.vidal.fr/parapharmacie/vidal/produits-id14775n0-CERAVE-CREME-HYDRATANTE-VISAGE.html
[CICALFATE]: https://eurekasante.vidal.fr/parapharmacie/vidal/produits-vf410015n2-CICALFATE-Creme.html

## Allergies

- Antiallergique [XYZALL] / [CÉTIRIZINE] / [LÉVOCÉTIRIZINE] / [DESLORATADINE] / [AERIUS] / [INORIAL]

[AERIUS]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp706-AERIUS.html
[CÉTIRIZINE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp1540-CETIRIZINE-BIOGARAN.html
[DESLORATADINE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp4798-DESLORATADINE-BIOGARAN.html
[INORIAL]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp4720-INORIAL.html
[LÉVOCÉTIRIZINE]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp4623-LEVOCETIRIZINE-BIOGARAN.html
[XYZALL]: https://eurekasante.vidal.fr/medicaments/vidal-famille/medicament-gp933-XYZALL.html 

# IMPORTANT ! 

> Ne pas oublier les cartes VITALE,
> ainsi que les traitements en cours s'il y a lieu.

 
**DANS TOUS LES CAS :** Consulter un médecin en cas d'urgence ou de doute.
